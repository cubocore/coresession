/*
	*
	* This file is a part of CoreSession.
	* CoreSession is a Session manager for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#pragma once

#include <QObject>
#include <QString>
#include <QX11Info>
#include <cprime/settingsmanage.h>

class CoreSession : public QObject {
	Q_OBJECT

	public:
		/* Init */
		CoreSession();

	public Q_SLOTS:
		/* Start the session by performing various tasks */
		void start( QString session = QString() );

		/* Stop the session by saving the state */
		void stop();

		/* Intitiate quit */
		void handleMessages( const QString );

	private:
		/* Start CoreApps */
		void startCoreApps();

		/* Start desktop: @desktop - full path to .desktop file */
		void startDesktop( QString desktop );

		/* Start scripts: @script - full path to the script/exec to be run */
		void startScript( QString script );

		/* Enable num lock */
		void enableNumLock();
};
