TEMPLATE = app
TARGET = coresession
INCLUDEPATH += .

QT += widgets x11extras

LIBS += -lX11 -lcprime

# So we can build locally
INCLUDEPATH += ../libcorex11
LIBS += -L../libcorex11 -lcorex11

# Input
HEADERS += coresession.h
SOURCES += coresession.cpp main.cpp

# Silent compilation
CONFIG += silent

# Build section
BUILD_PREFIX = $$(CA_BUILD_DIR)

isEmpty( BUILD_PREFIX ) {
	BUILD_PREFIX = ./build
}

MOC_DIR       = $$BUILD_PREFIX/moc-qt5
OBJECTS_DIR   = $$BUILD_PREFIX/obj-qt5
RCC_DIR       = $$BUILD_PREFIX/qrc-qt5
UI_DIR        = $$BUILD_PREFIX/uic-qt5

unix {
	isEmpty(PREFIX) {
		PREFIX = /usr
	}

	BINDIR        = $$PREFIX/bin

	target.path   = $$BINDIR

	desktop.path  = $$PREFIX/share/xsessions/
	desktop.files = "coresession.desktop"

	INSTALLS     += target desktop
}
