/*
	*
	* This file is a part of CoreSession.
	* CoreSession is a Session manager for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include <QDir>
#include <QProcess>

#include <cprime/filefunc.h>
#include <cprime/corexdg.h>
#include <X11/Xlib.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include "fixx11h.h"

#include "coresession.h"
#include "libcorex11.h"

CoreSession::CoreSession() {

};

void CoreSession::start( QString ) {

	/* Start WM, CoreStuff and other coreapps */
	startCoreApps();

	/* Enable the num lock on login */
	enableNumLock();

	return;

	/* Start apps from /etc/xdg/autostart */
	QDir xdgAutoDir( "/etc/xdg/autostart" );
	Q_FOREACH( QString desktop, xdgAutoDir.entryList( QStringList() << "*.desktop" ) ) {

		/* If the same file exists in user's autostart, skip it */
		if ( CPrime::FileUtils::exists( QDir::home().filePath( ".config/autostart/" + desktop ) ) )
			continue;

		startDesktop( xdgAutoDir.filePath( desktop ) );
	}

	/* Start apps from ~/.config/autostart */
	QDir homeAutoDir( QDir::home().filePath( ".config/autostart/" ) );
	Q_FOREACH( QString desktop, homeAutoDir.entryList( QStringList() << "*.desktop" ) )
		startDesktop( homeAutoDir.filePath( desktop ) );

	/* Start apps from ~/.config/autostart */
	QDir scriptAutoDir( QDir::home().filePath( ".config/autostart-scripts/" ) );
	Q_FOREACH( QString script, scriptAutoDir.entryList( QDir::Files ) )
		startScript( scriptAutoDir.filePath( script ) );
};

void CoreSession::stop() {

	CoreX11Session *x11 = CoreX11Session::currentSession();

	QSettings session( "coreapps", "session" );
	session.remove( "Previous" );
	Q_FOREACH( Window wID, x11->listClients() ) {
		CoreClient client( wID );

		/* We need to store the client information. For this we use /proc/<pid> */
		int pid = client.pid();
		QDir cProc( "/proc/" + QString::number( pid ) );
		if ( cProc.exists() ) {
			QString exec = CPrime::FileUtils::readLink( cProc.filePath( "exe" ) );
			QStringList cmdline;
			QFile cli( cProc.filePath( "cmdline" ) );
			if ( not cli.open( QFile::ReadOnly ) ) {
				qDebug() << "Unable to get data about" << client.title() << client.wid() << client.pid();
				continue;
			}

			Q_FOREACH( QByteArray part, cli.readAll().split( '\x00' ) ) {
				if ( part.count() )
					cmdline << QString::fromLocal8Bit( part );
			}

			if ( cmdline.count() )
				session.setValue( "Previous/" + exec, cmdline );
		}

		client.close();
	}

	qApp->quit();
}

void CoreSession::handleMessages( const QString msg ) {

	if ( msg == "--quit" ) {
		stop();
	}

	else if ( msg.startsWith( "--session" ) ) {
		QStringList args = msg.split( " ", QString::SkipEmptyParts );
		if ( args.count() == 2 )
			start( args[ 1 ] );

		else if ( args.count() == 1 )
			start();
	}
}

void CoreSession::startCoreApps() {

	/* First we start the window manager */
	QString wm = settingsManage::initialize()->value( "CoreSession", "WindowManager" );
	qDebug() << "Starting window manager" << wm;
	if ( not wm.isEmpty() )
		QProcess::startDetached( wm );

	/* Start corestuff, assuming it exists */
	qDebug() << "Starting corestuff";
	QProcess::startDetached( "corestuff" );

	/* Start CoreTime */
	qDebug() << "Starting coretime";
	QProcess::startDetached( "coretime", QStringList() << "--tray" );

	/* Start other apps set in CoreGarage */
	QSettings autoSett( "coreapps", "coreapps" );
	Q_FOREACH( QString app, autoSett.value( "CoreSession/AutoStartList" ).toStringList() ) {
		CoreDesktopFile df( app );
		df.startApplicationWithArgs( QStringList() );
	}
};

void CoreSession::startDesktop( QString desktop ) {

	QSettings desktopFile( desktop, QSettings::IniFormat );

	/* If the desktop file is not to be opened in CuboCore, skip it */
	/* Example: cinnamon-settings-daemon-*.desktop should not be opened */
	if ( desktopFile.contains( "Desktop Entry/OnlyShowIn" ) ) {
		if ( desktopFile.value( "Desktop Entry/OnlyShowIn" ).toString() != "CuboCore" )
			return;
	}

	/* If an application is set to be Hidden; do not open it */
	if ( desktopFile.value( "Desktop Entry/Hidden" ).toBool() == true )
		return;

	/* If the TryExec is valid, execute it */
	QString exec = desktopFile.value( "Desktop Entry/TryExec" ).toString();
	if ( not exec.isEmpty() ) {
		if ( CPrime::FileUtils::exists( exec ) ) {
			QProcess::startDetached( exec );
			return;
 		}

		else {
			Q_FOREACH( QString path, qgetenv( "PATH" ).split( ':' ) ) {
				if ( CPrime::FileUtils::exists( path + "/" + exec ) ) {
					QProcess::startDetached( exec );
					return;
				}
			}
		}

		/* If TryExec is set, but not valid do not start the app */
		return;
	}

	/* If Exec is valid; execute it */
	exec = desktopFile.value( "Desktop Entry/Exec" ).toString();
	if ( not exec.isEmpty() ) {
		if ( CPrime::FileUtils::exists( exec ) ) {
			QProcess::startDetached( exec );
			return;
		}

		else {
			Q_FOREACH( QString path, qgetenv( "PATH" ).split( ':' ) ) {
				if ( CPrime::FileUtils::exists( path + "/" + exec ) ) {
					QProcess::startDetached( exec );
					return;
				}
			}
		}

		return;
	}
};

void CoreSession::startScript( QString script ) {

	/* @script can be a link, get the real path */
	QString realScript = CPrime::FileUtils::readLink( script );
	qDebug() << "Starting script" << script;
	QProcess::startDetached( realScript );
};

void CoreSession::enableNumLock() {

	qDebug() << "Enabling NumLock";

	Display *dpy = QX11Info::display();

	unsigned int mask = 0;
	XkbDescPtr xkb = XkbGetKeyboard( dpy, XkbAllComponentsMask, XkbUseCoreKbd );
	if( xkb and xkb->names ) {
		for( int i = 0; i < XkbNumVirtualMods; i++ ) {
			char* modStr = XGetAtomName( xkb->dpy, xkb->names->vmods[ i ] );
			if( modStr != nullptr && strcmp( "NumLock", modStr ) == 0 ) {
				unsigned int msk;
				XkbVirtualModsToReal( xkb, 1 << i, &msk );
				mask = msk;
				break;
			}
		}

		if ( mask )
			XkbLockModifiers( dpy, XkbUseCoreKbd, mask, mask );
	}
};
