/*
	*
	* This file is a part of CoreSession.
	* CoreSession is a Session manager for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include <QDir>

#include <cprime/capplication.h>
#include "coresession.h"

void Logger( QtMsgType type, const QMessageLogContext &context, const QString &message ) {

	Q_UNUSED( context );

	/*
		*
		* Define nblog
		*
	*/
	QString logPath = QDir::home().filePath( ".config/coreapps/coresession.log" );
	FILE *nblog = fopen( logPath.toLocal8Bit().data(), "a" );

	switch ( type ) {
		case QtInfoMsg: {
			fprintf( nblog, "CoreSession::Debug# %s\n", message.toLocal8Bit().data() );
			fflush( nblog );
			fprintf( stderr, "\033[01;32mCoreSession::Info# %s\n\033[00;00m", message.toLocal8Bit().data() );
			break;
		}

		case QtDebugMsg: {
			fprintf( nblog, "CoreSession::Debug# %s\n", message.toLocal8Bit().data() );
			fflush( nblog );
			fprintf( stderr, "\033[01;30mCoreSession::Debug# %s\n\033[00;00m", message.toLocal8Bit().data() );
			break;
		}

		case QtWarningMsg: {
			fprintf( nblog, "CoreSession::Warning# %s\n", message.toLocal8Bit().data() );
			fflush( nblog );
			if ( QString( message ).contains( "X Error" ) or QString( message ).contains( "libpng warning" ) )
				break;
			fprintf( stderr, "\033[01;33mCoreSession::Warning# %s\n\033[00;00m", message.toLocal8Bit().data() );
			break;
		}

		case QtCriticalMsg: {
			fprintf( nblog, "CoreSession::CriticalError# %s\n", message.toLocal8Bit().data() );
			fflush( nblog );
			fprintf( stderr, "\033[01;31mCoreSession::CriticalError# %s\n\033[00;00m", message.toLocal8Bit().data() );
			break;
		}

		case QtFatalMsg: {
			fprintf( nblog, "CoreSession::FatalError# %s\n", message.toLocal8Bit().data() );
			fflush( nblog );
			fprintf( stderr, "\033[01;41mCoreSession::FatalError# %s\n\033[00;00m", message.toLocal8Bit().data() );
			abort();
		}
	}
};

int main( int argc, char *argv[] ) {

	qInstallMessageHandler( Logger );

	#if QT_VERSION >= QT_VERSION_CHECK(5, 6, 0)
		QApplication::setAttribute( Qt::AA_EnableHighDpiScaling );
	#endif

    CApplication app( "CoreSession", argc, argv );
    app.setQuitOnLastWindowClosed( false );

    // Set application info
    app.setOrganizationName( "CuboCore" );
    app.setApplicationName( "CoreSession" );

    CoreSession *session = new CoreSession();
    QObject::connect( &app, SIGNAL( messageReceived( const QString ) ), session, SLOT( handleMessages( const QString ) ) );

    if ( app.isRunning() and ( argc >= 2 ) ) {
		if ( app.arguments().contains( "--quit" ) ) {
			qDebug() << "Trying to quit" << app.sendMessage( "--quit" );
			return 0;
		}

		else if ( strcmp( argv[ 1 ], "--session" ) == 0 ) {
			QStringList args = app.arguments();
			args.pop_front();
			app.sendMessage( args.join( " " ) );
		}
	}

    session->start();

    return app.exec();
}
