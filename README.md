# CoreSession
A basic session manager that can work with any Window Manager. Currently x11 is supported.


### Download
You can download latest release.
* [Source](https://gitlab.com/cubocore/coresession/tags)
* [ArchPackages](https://gitlab.com/cubocore/wiki/tree/master/ArchPackages)

### Dependencies:
* qt5-base
* qt5-x11extras
* xcb-util-wm (libxcb-dev & libxcb1-dev)
* [libcprime](https://gitlab.com/cubocore/libcprime)


### Information
Please see the Wiki page for this info.[Wiki page](https://gitlab.com/cubocore/wiki) .
* Changes in latest release
* Build from the source
* Tested In
* Known Bugs
* Help Us

### Feedback
* We need your feedback to improve the CoreApps.Send us your feedback through GitLab [issues](https://gitlab.com/groups/cubocore/-/issues).



