/*
	*
	* This file is a part of CoreSession.
	* CoreSession is a Session manager for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#pragma once

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <locale.h>
#include <xcb/xcb.h>
#include <xcb/xproto.h>
#include <xcb/xcb_atom.h>
#include <xcb/xcb_ewmh.h>
#include <xcb/xcb_icccm.h>
#include <iostream>
#include <list>

#define _NET_WM_STATE_REMOVE        0    /* remove/unset property */
#define _NET_WM_STATE_ADD           1    /* add/set property */
#define _NET_WM_STATE_TOGGLE        2    /* toggle property  */

#define MAX_PROPERTY_VALUE_LEN 4096
#define SELECT_WINDOW_MAGIC ":SELECT:"
#define ACTIVE_WINDOW_MAGIC ":ACTIVE:"

#include <QtCore>
#include <QtGui>

typedef unsigned long int qulong;
typedef unsigned long int Window;
typedef QList<Window> Windows;

class X11KeyParser {

	public:
		static quint32 calcId( const QKeySequence keyseq );
		static quint32 calcId( quint32 k, quint32 m );

		static Qt::Key getKey( const QKeySequence keyseq );
		static Qt::KeyboardModifiers getMods( const QKeySequence keyseq );

		static quint32 toNativeKeycode( Qt::Key k );
		static quint32 toNativeModifiers( Qt::KeyboardModifiers m );
};

class CoreClient : public QObject {

	Q_OBJECT

public:
    enum ClientType {
        None					= 0x000000,
        Desktop					= 0x11D8E4,
        Dock,
        Toolbar,
        Menu,
        Utility,
        Splash,
        Dialog,
        Dropdown_menu,
        Popup_menu,
        Tooltip,
        Notification,
        Combo,
        Dnd,
        Normal
    };

    CoreClient( Window wid );
    CoreClient( const CoreClient& );

    qulong type() const;
    qulong pid() const;
    qulong wid() const;
    int desktop() const;
    QString title() const;

    QIcon icon() const;
    QString typeStr() const;

    bool isMinimized() const;
    bool isMaximized() const;
    bool isActiveWindow() const;

public Q_SLOTS:
    void activate();
    void minimize();
    void maximize();
    void close();
    //void restore();

private:
    xcb_connection_t      *XCB;				// XCB Connection
    xcb_ewmh_connection_t EWMH;				// XCB EWMH Connection

    bool mEWMHValid;

    qulong mType = ClientType::Normal;		// Normal window
    qulong mWid = 0;						// Window ID
    uint32_t mPid = 0;						// Process ID
    uint32_t mDesktop = 0;                  // Desktop Number
    QString mTitle;							// Window Title
    QIcon mIcon;							// Client icon

    void getIcon();
    void getType();

};

class CoreX11Session : public QObject {

    Q_OBJECT

public:
    /* Current session instance */
    static CoreX11Session *currentSession();

    /* Window ID of the active window */
    Window getActiveWindow();

    /* List all the clients */
    Windows listClients();

    /* Size of the desktop */
    QSize desktopSize();

private:
    CoreX11Session();
    static CoreX11Session *mCurSession;

    xcb_connection_t *mConn;
    xcb_ewmh_connection_t EWMH;

    bool mEWMHValid;

Q_SIGNALS:
    void listWindows();
    void activeWindowChanged();
	void resizeDesktop();

	void activateDock();
};

class CoreXCBEventFilter : public QAbstractNativeEventFilter, QObject {

public:
    CoreXCBEventFilter( CoreX11Session *session );

    virtual bool nativeEventFilter(const QByteArray &eventType, void *message, long *) Q_DECL_OVERRIDE;

private:
    QList<xcb_atom_t> WinNotifyAtoms, SysNotifyAtoms;
    xcb_ewmh_connection_t EWMH;

    CoreX11Session *mSession;
};
