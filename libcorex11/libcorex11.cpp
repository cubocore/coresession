/*
	*
	* This file is a part of CoreX11Session.
	* CoreX11Session is a Session manager for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include <X11/Xlib.h>
#include <X11/keysym.h>

#include "fixx11h.h"

#include <QtCore>
#include <QtGui>

#include <QX11Info>

#include <cstdio>
#include "libcorex11.h"

quint32 X11KeyParser::calcId( const QKeySequence keyseq ) {

    quint32 keycode = toNativeKeycode( getKey( keyseq ) );
    quint32 mods    = toNativeModifiers( getMods( keyseq ) );

    return calcId( keycode, mods );
};

quint32 X11KeyParser::calcId( quint32 k, quint32 m ) {

    return k | ( m << 16 );
};

Qt::Key X11KeyParser::getKey( const QKeySequence keyseq ) {
    if ( keyseq.isEmpty() ) {
        return Qt::Key( 0 );
    }

    return Qt::Key( keyseq[ 0 ] & ~Qt::KeyboardModifierMask );
};

Qt::KeyboardModifiers X11KeyParser::getMods( const QKeySequence keyseq ) {

    if ( keyseq.isEmpty() ) {
        return Qt::KeyboardModifiers( 0 );
    }

    return Qt::KeyboardModifiers( keyseq[ 0 ] & Qt::KeyboardModifierMask );
};

quint32 X11KeyParser::toNativeKeycode( Qt::Key k ) {

    /* keysymdef.h */
    quint32 key = 0;
    if (k >= Qt::Key_F1 && k <= Qt::Key_F35) {
        key = XK_F1 + (k - Qt::Key_F1);
    } else if (k >= Qt::Key_Space && k <= Qt::Key_QuoteLeft) {
        key = k;
    } else if (k >= Qt::Key_BraceLeft && k <= Qt::Key_AsciiTilde) {
        key = k;
    } else if (k >= Qt::Key_nobreakspace && k <= Qt::Key_ydiaeresis) {
        key = k;
    } else {
        switch (k) {
        case Qt::Key_Escape:
            key = XK_Escape;
            break;
        case Qt::Key_Tab:
        case Qt::Key_Backtab:
            key = XK_Tab;
            break;
        case Qt::Key_Backspace:
            key = XK_BackSpace;
            break;
        case Qt::Key_Meta:
        case Qt::Key_Super_L:
        case Qt::Key_Super_R:
            key = XK_Meta_L;
            break;
        case Qt::Key_Return:
        case Qt::Key_Enter:
            key = XK_Return;
            break;
        case Qt::Key_Insert:
            key = XK_Insert;
            break;
        case Qt::Key_Delete:
            key = XK_Delete;
            break;
        case Qt::Key_Pause:
            key = XK_Pause;
            break;
        case Qt::Key_Print:
            key = XK_Print;
            break;
        case Qt::Key_SysReq:
            key = XK_Sys_Req;
            break;
        case Qt::Key_Clear:
            key = XK_Clear;
            break;
        case Qt::Key_Home:
            key = XK_Home;
            break;
        case Qt::Key_End:
            key = XK_End;
            break;
        case Qt::Key_Left:
            key = XK_Left;
            break;
        case Qt::Key_Up:
            key = XK_Up;
            break;
        case Qt::Key_Right:
            key = XK_Right;
            break;
        case Qt::Key_Down:
            key = XK_Down;
            break;
        case Qt::Key_PageUp:
            key = XK_Page_Up;
            break;
        case Qt::Key_PageDown:
            key = XK_Page_Down;
            break;
        default:
            key = 0;
        }
    }
    return XKeysymToKeycode(QX11Info::display(), key);
};

quint32 X11KeyParser::toNativeModifiers(Qt::KeyboardModifiers m) {

    quint32 mods = Qt::NoModifier;
    if (m & Qt::ShiftModifier)
        mods |= ShiftMask;
    if (m & Qt::ControlModifier)
        mods |= ControlMask;
    if (m & Qt::AltModifier)
        mods |= Mod1Mask;
    if (m & Qt::MetaModifier)
        mods |= Mod4Mask;
    return mods;
}

QString GetAtomName( xcb_connection_t *disp, xcb_atom_t atm ) {

    xcb_get_atom_name_reply_t *reply = xcb_get_atom_name_reply( disp, xcb_get_atom_name( disp, atm ), nullptr );
	const char *name = xcb_get_atom_name_name( reply );

	return QString::fromLocal8Bit( name );
};

xcb_atom_t GetAtom( xcb_connection_t *disp, const char *name ) {

    xcb_intern_atom_cookie_t atom_cookie;
    xcb_atom_t atom = XCB_ATOM_NONE;
    xcb_intern_atom_reply_t *rep;

    atom_cookie = xcb_intern_atom( disp, 0, strlen( name ), name );
    rep = xcb_intern_atom_reply( disp, atom_cookie, nullptr );
    if ( nullptr != rep ) {
        atom = rep->atom;
        free( rep );
    }

    return atom;
};

CoreClient::CoreClient( Window WId ) : QObject(), mWid( WId ) {

	XCB = QX11Info::connection();

	xcb_intern_atom_cookie_t *cookie = xcb_ewmh_init_atoms( XCB, &EWMH );
    mEWMHValid = static_cast<bool>(xcb_ewmh_init_atoms_replies( &EWMH, cookie, nullptr));

	/* PID and Desktop number */
    xcb_ewmh_get_wm_pid_reply( &EWMH, xcb_ewmh_get_wm_pid( &EWMH, WId ), &mPid, nullptr );
    xcb_ewmh_get_wm_desktop_reply( &EWMH, xcb_ewmh_get_wm_desktop( &EWMH, WId ), &mDesktop, nullptr );

	/* Title */
	xcb_get_property_cookie_t netWMameCookie = xcb_get_property( XCB, false, WId, EWMH._NET_WM_NAME, EWMH.UTF8_STRING, 0, BUFSIZ );
    xcb_get_property_reply_t *replyNetWM = xcb_get_property_reply( XCB, netWMameCookie, nullptr );

	char *wm_name = {};
	int wm_name_len = 0;

	if ( replyNetWM && ( replyNetWM->type != XCB_NONE ) ) {
        wm_name = static_cast<char *>(xcb_get_property_value( replyNetWM ));
		wm_name_len = xcb_get_property_value_length( replyNetWM );
	}

	else {
		xcb_get_property_cookie_t nameCookie = xcb_get_property( XCB, false, WId, EWMH._NET_WM_VISIBLE_NAME, EWMH.UTF8_STRING, 0, BUFSIZ );
        xcb_get_property_reply_t *reply = xcb_get_property_reply( XCB, nameCookie, nullptr );

		if ( reply && ( reply->type != XCB_NONE ) ) {
            wm_name = static_cast<char *>(xcb_get_property_value( reply ));
			wm_name_len = xcb_get_property_value_length( reply );
		}
	}

	if ( wm_name_len > 0 )
		mTitle = QString::fromLocal8Bit( wm_name, wm_name_len );

	getIcon();
	getType();
};

CoreClient::CoreClient( const CoreClient& other ) : QObject() {

	mType = other.type();
	mPid = other.pid();
	mDesktop = other.desktop();
	mWid = other.wid();
	mIcon = other.icon();
	mTitle = other.title();
};

qulong CoreClient::type() const {

	return mType;
};

qulong CoreClient::pid() const {

	return mPid;
};

qulong CoreClient::wid() const {

	return mWid;
};

int CoreClient::desktop() const {

	return mDesktop;
};

QString CoreClient::title() const {

	return mTitle;
};

QIcon CoreClient::icon() const {

	return mIcon;
};

QString CoreClient::typeStr() const {

	switch( mType ) {
		case None:
			return QString();

		case Desktop:
			return QString( "Desktop" );

		case Dock:
			return QString( "Dock" );

		case Toolbar:
			return QString( "ToolBar" );

		case Menu:
			return QString( "Menu" );

		case Utility:
			return QString( "Utility" );

		case Splash:
			return QString( "SplashScreen" );

		case Dialog:
			return QString( "Dialog" );

		case Dropdown_menu:
			return QString( "DropDownMenu" );

		case Popup_menu:
			return QString( "PopupMenu" );

		case Tooltip:
			return QString( "Tooltip" );

		case Notification:
			return QString( "Notification" );

		case Combo:
			return QString( "Combo" );

		case Dnd:
			return QString( "DnD" );

		case Normal:
			return QString( "Normal" );
	}

	return QString( "Normal" );
};

bool CoreClient::isMinimized() const {

	xcb_atom_t wm_state = GetAtom( XCB, "_NET_WM_STATE" );
	xcb_get_property_cookie_t stateCookie = xcb_get_property( XCB, false, mWid, wm_state, XCB_GET_PROPERTY_TYPE_ANY, 0, BUFSIZ );
    xcb_get_property_reply_t *reply = xcb_get_property_reply( XCB, stateCookie, nullptr );

	if ( reply && ( reply->type != XCB_NONE ) && ( reply->value_len > 0 ) ) {
        xcb_atom_t *atoms = static_cast<xcb_atom_t * >(xcb_get_property_value( reply ));
	    int numItems = reply->value_len;

		for( int i = 0; i < numItems; i++ ) {
			if ( atoms[ i ] == EWMH._NET_WM_STATE_HIDDEN )
				return true;
		}
	}

	return false;
};

bool CoreClient::isMaximized() const {

	xcb_atom_t wm_state = GetAtom( XCB, "_NET_WM_STATE" );
	xcb_get_property_cookie_t stateCookie = xcb_get_property( XCB, false, mWid, wm_state, XCB_GET_PROPERTY_TYPE_ANY, 0, BUFSIZ );
    xcb_get_property_reply_t *reply = xcb_get_property_reply( XCB, stateCookie, nullptr );

	bool min = false, maxv = false, maxh = false;
	if ( reply && ( reply->type != XCB_NONE ) && ( reply->value_len > 0 ) ) {
        xcb_atom_t *atoms = static_cast<xcb_atom_t * >(xcb_get_property_value( reply ));
	    int numItems = reply->value_len;

		for( int i = 0; i < numItems; i++ ) {
			if ( atoms[ i ] == EWMH._NET_WM_STATE_HIDDEN )
				min = true;

			if ( atoms[ i ] == EWMH._NET_WM_STATE_MAXIMIZED_HORZ )
				maxh = true;

			if ( atoms[ i ] == EWMH._NET_WM_STATE_MAXIMIZED_VERT )
				maxv = true;
		}
	}

	/* We have a maximized window if its not minimized and has V and H maximized */
	return ( not min and ( maxv and maxh ) );
};

bool CoreClient::isActiveWindow() const {

	Window focus = CoreX11Session::currentSession()->getActiveWindow();

	if ( focus == mWid )
		return true;

	return false;
};

void CoreClient::activate() {

	xcb_ewmh_request_change_active_window( &EWMH, QX11Info::appScreen(), mWid, XCB_EWMH_CLIENT_SOURCE_TYPE_NORMAL, 0, 0 );
};

void CoreClient::minimize() {

    xcb_client_message_event_t event;

	event.response_type = XCB_CLIENT_MESSAGE;
	event.format = 32;
	event.sequence = 0;
	event.window = mWid;
	event.type = GetAtom( XCB, "WM_CHANGE_STATE" );
	event.data.data32[0] = XCB_ICCCM_WM_STATE_ICONIC;
	event.data.data32[1] = 0;
	event.data.data32[2] = 0;
	event.data.data32[3] = 0;
	event.data.data32[4] = 0;

    xcb_send_event( XCB, 0, QX11Info::appRootWindow(), XCB_EVENT_MASK_STRUCTURE_NOTIFY | XCB_EVENT_MASK_SUBSTRUCTURE_REDIRECT, (const char *)&event );
};

/*void CoreClient::restore() {

    // If minimized, lets first activate it, and then perform the toggle
	if ( isMinimized() ) {

		xcb_client_message_event_t event;

		event.response_type = XCB_CLIENT_MESSAGE;
		event.format = 32;
		event.sequence = 0;
		event.window = mWid;
		event.type = GetAtom( XCB, "WM_CHANGE_STATE" );
		event.data.data32[0] = XCB_ICCCM_WM_STATE_NORMAL;
		event.data.data32[1] = 0;
		event.data.data32[2] = 0;
		event.data.data32[3] = 0;
		event.data.data32[4] = 0;

		xcb_send_event( XCB, 0, QX11Info::appRootWindow(), XCB_EVENT_MASK_STRUCTURE_NOTIFY | XCB_EVENT_MASK_SUBSTRUCTURE_REDIRECT, (const char *)&event );
	}

	else {
		xcb_ewmh_request_change_wm_state(
			&EWMH,
			QX11Info::appScreen(),
			mWid,
			XCB_EWMH_WM_STATE_REMOVE,
			EWMH._NET_WM_STATE_MAXIMIZED_VERT,
			EWMH._NET_WM_STATE_MAXIMIZED_HORZ,
			XCB_EWMH_CLIENT_SOURCE_TYPE_NORMAL
		);
	}
};*/

void CoreClient::maximize() {

	// /* If minimized, lets first activate it, and then perform the toggle */
	if ( isMinimized() ) {

		xcb_client_message_event_t event;

		event.response_type = XCB_CLIENT_MESSAGE;
		event.format = 32;
		event.sequence = 0;
		event.window = mWid;
		event.type = GetAtom( XCB, "WM_CHANGE_STATE" );
		event.data.data32[0] = XCB_ICCCM_WM_STATE_NORMAL;
		event.data.data32[1] = 0;
		event.data.data32[2] = 0;
		event.data.data32[3] = 0;
		event.data.data32[4] = 0;

		xcb_send_event( XCB, 0, QX11Info::appRootWindow(), XCB_EVENT_MASK_STRUCTURE_NOTIFY | XCB_EVENT_MASK_SUBSTRUCTURE_REDIRECT, (const char *)&event );
	}

	xcb_ewmh_request_change_wm_state(
		&EWMH,
		QX11Info::appScreen(),
		mWid,
		XCB_EWMH_WM_STATE_ADD,
		EWMH._NET_WM_STATE_MAXIMIZED_VERT,
		EWMH._NET_WM_STATE_MAXIMIZED_HORZ,
		XCB_EWMH_CLIENT_SOURCE_TYPE_NORMAL
	);
};

void CoreClient::close() {

	xcb_ewmh_request_close_window(
		&EWMH,
		QX11Info::appScreen(),
		mWid,
		0,
		XCB_EWMH_CLIENT_SOURCE_TYPE_NORMAL
	);
};

void CoreClient::getIcon() {

	mIcon.addPixmap( QIcon::fromTheme( "application-x-executable" ).pixmap( 48 ) );

	xcb_get_property_cookie_t cookie = xcb_ewmh_get_wm_icon_unchecked( &EWMH, mWid );
	xcb_ewmh_get_wm_icon_reply_t reply;

    if ( xcb_ewmh_get_wm_icon_reply( &EWMH, cookie, &reply, nullptr ) ) {
		xcb_ewmh_wm_icon_iterator_t iter = xcb_ewmh_get_wm_icon_iterator(&reply);

		//Just use the first
		bool done = false;
		while ( not done ) {
			//Now convert the current data into a Qt image
			// - first 2 elements are width and height (removed via XCB functions)
			// - data in rows from left to right and top to bottom
			QImage image( iter.width, iter.height, QImage::Format_ARGB32 ); //initial setup
			uint* dat = iter.data;

			#if QT_VERSION <= QT_VERSION_CHECK(5, 10, 0)
				for ( int i = 0; i < image.byteCount() / 4; ++i, ++dat )
			#else
				for ( int i = 0; i < image.sizeInBytes() / 4; ++i, ++dat )
			#endif
					( ( uint* )image.bits() )[ i ] = *dat;

			mIcon.addPixmap( QPixmap::fromImage( image ) );

			// icon.addPixmap( QPixmap::fromImage( image ) ); //layer this pixmap onto the icon
			//Now see if there are any more icons available
			done = ( iter.rem < 1 ); //number of icons remaining
			if ( not done )
				xcb_ewmh_get_wm_icon_next( &iter ); //get the next icon data
		}

		xcb_ewmh_get_wm_icon_reply_wipe( &reply );
	}
};

void CoreClient::getType() {

    xcb_atom_t propDesktop			= GetAtom( XCB, static_cast<const char * >( "_NET_WM_WINDOW_TYPE_DESKTOP" ));
    xcb_atom_t propDock				= GetAtom( XCB, static_cast<const char * >( "_NET_WM_WINDOW_TYPE_DOCK" ));
    xcb_atom_t propToolbar			= GetAtom( XCB, static_cast<const char * >( "_NET_WM_WINDOW_TYPE_TOOLBAR" ));
    xcb_atom_t propMenu				= GetAtom( XCB, static_cast<const char * >( "_NET_WM_WINDOW_TYPE_MENU" ));
    xcb_atom_t propUtility			= GetAtom( XCB, static_cast<const char * >( "_NET_WM_WINDOW_TYPE_UTILITY" ));
    xcb_atom_t propSplash			= GetAtom( XCB, static_cast<const char * >( "_NET_WM_WINDOW_TYPE_SPLASH" ));
    xcb_atom_t propDialog			= GetAtom( XCB, static_cast<const char * >( "_NET_WM_WINDOW_TYPE_DIALOG" ));
    xcb_atom_t propDropdown_menu	= GetAtom( XCB, static_cast<const char * >( "_NET_WM_WINDOW_TYPE_DROPDOWN_MENU" ));
    xcb_atom_t propPopup_menu		= GetAtom( XCB, static_cast<const char * >( "_NET_WM_WINDOW_TYPE_POPUP_MENU" ));
    xcb_atom_t propTooltip			= GetAtom( XCB, static_cast<const char * >( "_NET_WM_WINDOW_TYPE_TOOLTIP" ));
    xcb_atom_t propNotification 	= GetAtom( XCB, static_cast<const char * >( "_NET_WM_WINDOW_TYPE_NOTIFICATION" ));
    xcb_atom_t propCombo			= GetAtom( XCB, static_cast<const char * >( "_NET_WM_WINDOW_TYPE_COMBO" ));
    xcb_atom_t propDnd				= GetAtom( XCB, static_cast<const char * >( "_NET_WM_WINDOW_TYPE_DND" ));
    xcb_atom_t propNormal			= GetAtom( XCB, static_cast<const char * >( "_NET_WM_WINDOW_TYPE_NORMAL" ));

	xcb_atom_t atom = GetAtom( XCB, "_NET_WM_WINDOW_TYPE" );
	xcb_get_property_cookie_t cookie = xcb_get_property( XCB, false, mWid, atom, XCB_GET_PROPERTY_TYPE_ANY, 0, BUFSIZ );
    xcb_get_property_reply_t *reply = xcb_get_property_reply( XCB, cookie, nullptr );

	if ( reply && ( reply->type != XCB_NONE ) && ( reply->value_len > 0 ) ) {
        xcb_atom_t *atoms = static_cast<xcb_atom_t * >(xcb_get_property_value( reply ));
	    int numItems = reply->value_len;

		/*
			* Multiple window types can be set, example, Frameless Qt Window in KDE
			* KDE sets _KDE_NET_WM_WINDOW_TYPE_OVERRIDE and _NET_WM_WINDOW_TYPE_NORMAL
			* In such cases we look for and use the first defined type
		*/
		for( int i = 0; i < numItems; i++ ) {
			xcb_atom_t pprty = atoms[ i ];

			if ( pprty == propDesktop )
				mType = Desktop;

			else if ( pprty == propDock )
				mType = Dock;

			else if ( pprty == propToolbar )
				mType = Toolbar;

			else if ( pprty == propMenu )
				mType = Menu;

			else if ( pprty == propUtility )
				mType = Utility;

			else if ( pprty == propSplash )
				mType = Splash;

			else if ( pprty == propDialog )
				mType = Dialog;

			else if ( pprty == propDropdown_menu )
				mType = Dropdown_menu;

			else if ( pprty == propPopup_menu )
				mType = Popup_menu;

			else if ( pprty == propTooltip )
				mType = Tooltip;

			else if ( pprty == propNotification )
				mType = Notification;

			else if ( pprty == propCombo )
				mType = Combo;

			else if ( pprty == propDnd )
				mType = Dnd;

			else if ( pprty == propNormal )
				mType = Normal;

			if ( mType != None )
				break;
		}
	}
};

CoreX11Session *CoreX11Session::mCurSession = nullptr;

CoreX11Session* CoreX11Session::currentSession() {

	if ( not mCurSession )
		mCurSession = new CoreX11Session();

	return mCurSession;
};

CoreX11Session::CoreX11Session() : QObject() {

	mConn = QX11Info::connection();

	xcb_intern_atom_cookie_t *cookie = xcb_ewmh_init_atoms( QX11Info::connection(), &EWMH );
    mEWMHValid = static_cast<bool>(xcb_ewmh_init_atoms_replies( &EWMH, cookie, nullptr ));
};

Window CoreX11Session::getActiveWindow() {

	if ( not mEWMHValid )
		return 0;

	xcb_window_t window = 0;

	xcb_get_property_cookie_t activeCookie = xcb_ewmh_get_active_window( &EWMH, QX11Info::appScreen() );
    xcb_ewmh_get_active_window_reply( &EWMH, activeCookie, &window, nullptr );

	return window;
};

Windows CoreX11Session::listClients() {

	if ( not mEWMHValid )
		return Windows();

	xcb_ewmh_get_windows_reply_t windows;

	xcb_get_property_cookie_t listCookie = xcb_ewmh_get_client_list( &EWMH, QX11Info::appScreen() );
    xcb_ewmh_get_windows_reply( &EWMH, listCookie, &windows, nullptr );

	Windows winList;

	for( uint i = 0; i < windows.windows_len; i++ )
		winList << windows.windows[ i ];

	return winList;
};

QSize CoreX11Session::desktopSize() {

	xcb_screen_t          *screen;
	int                    screen_nbr;
	xcb_screen_iterator_t  iter;

	screen_nbr = xcb_setup_roots_length( xcb_get_setup( mConn ) );

	/* Get the screen #screen_nbr */
	iter = xcb_setup_roots_iterator( xcb_get_setup( mConn ) );
	for( ; iter.rem; --screen_nbr, xcb_screen_next( &iter ) ) {
		if ( screen_nbr == 0 ) {
			screen = iter.data;
			break;
		}
	}

	return QSize( screen->width_in_pixels, screen->height_in_pixels );
};

CoreXCBEventFilter::CoreXCBEventFilter( CoreX11Session *sess ) : QAbstractNativeEventFilter(), mSession( sess ) {

	qWarning() << "Init XCB";

	xcb_intern_atom_cookie_t *cookie = xcb_ewmh_init_atoms( QX11Info::connection(), &EWMH );
    if ( not xcb_ewmh_init_atoms_replies( &EWMH, cookie, nullptr ) )
		qWarning() << "Error in XCB &EWMH init. You may not get any events at all.";

	uint32_t mask = XCB_EVENT_MASK_FOCUS_CHANGE | XCB_EVENT_MASK_PROPERTY_CHANGE | XCB_EVENT_MASK_KEY_PRESS;
	xcb_change_window_attributes( QX11Info::connection(), QX11Info::appRootWindow(), XCB_CW_EVENT_MASK, &mask );

	/* Watch all the events of the already open windows as well */
	Q_FOREACH( Window window, sess->listClients() )
		xcb_change_window_attributes( QX11Info::connection(), window, XCB_CW_EVENT_MASK, &mask );

	WinNotifyAtoms << EWMH._NET_WM_NAME << EWMH._NET_WM_VISIBLE_NAME << EWMH._NET_WM_ICON_NAME;
	WinNotifyAtoms << EWMH._NET_WM_VISIBLE_ICON_NAME << EWMH._NET_WM_ICON << EWMH._NET_WM_ICON_GEOMETRY;

	SysNotifyAtoms << EWMH._NET_CLIENT_LIST << EWMH._NET_CLIENT_LIST_STACKING << EWMH._NET_CURRENT_DESKTOP;
	SysNotifyAtoms << EWMH._NET_WM_STATE << EWMH._NET_ACTIVE_WINDOW;

	/* Grab the key sequence that activates our dock button */
	QSettings keySett( "coreapps", "coreapps" );
	QKeySequence kSeq = keySett.value( "CoreStuff/DockKeyCombo" ).value<QKeySequence>();

	/* Grab the meta key */
	xcb_grab_key( QX11Info::connection(), 1, QX11Info::appRootWindow(), 0, 133, XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC );

	if ( not kSeq.isEmpty() ) {
		quint32 m = X11KeyParser::toNativeModifiers( X11KeyParser::getMods( kSeq ) );
		quint32 k = X11KeyParser::toNativeKeycode( X11KeyParser::getKey( kSeq ) );
		xcb_grab_key( QX11Info::connection(), 1, QX11Info::appRootWindow(), m, k, XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC );
	}
};

/* This code is based on Qt5 documentation */
bool CoreXCBEventFilter::nativeEventFilter( const QByteArray &eventType, void *message, long * ) {

	if ( eventType == "xcb_generic_event_t" ) {

		/* Convert the event to xcb_generic_event_t* so that we can pasre it */
		xcb_generic_event_t *ev = static_cast<xcb_generic_event_t *>( message );

		/* Parse the above event and emit necessary signals */
		switch( ev->response_type & ~0x80 ) {

			/* Some property has changed */
			case XCB_PROPERTY_NOTIFY: {

				xcb_property_notify_event_t *pev = ( xcb_property_notify_event_t* )ev;

				if ( pev->window == QX11Info::appRootWindow() ) {
					if ( pev->atom == EWMH._NET_CURRENT_DESKTOP ) {
						// This means current desktop has changed.
						// qDebug() << "Current desktop changed";
					}

					else if ( ( pev->atom == EWMH._NET_DESKTOP_GEOMETRY ) or ( pev->atom == EWMH._NET_WORKAREA ) ) {
						// The desktop geometry has changed
						// Might be caused due to switch to external screen?
						qDebug() << "Desktop geometry/workarea changed";
						mSession->resizeDesktop();
					}

					else if ( SysNotifyAtoms.contains( pev->atom ) ) {
						// Property of some window has changed, active window
						// stacking order, icon, etcxcb_selection_clear_event_t
						// pev->window is the pointer to that window.

						if ( pev->atom == EWMH._NET_CLIENT_LIST ) {
							// qDebug() << "Window added or removed.";
							mSession->listWindows();
						}

						else if ( pev->atom == EWMH._NET_ACTIVE_WINDOW ) {
							// qDebug() << "Active window changed";
							mSession->activeWindowChanged();
						}
					}

					else if ( WinNotifyAtoms.contains( pev->atom ) ) {
						// Property of some window name or icon has changed
						// pev->window is the window id.
						// qDebug() << "Window changed (2):" << pev->window;
						// qDebug() << GetAtomName( QX11Info::connection(), pev->atom );
					}
				}

				else {
					// qDebug() << "Window changed (3):" << pev->window;
					// qDebug() << GetAtomName( QX11Info::connection(), pev->atom );
				}

				break;
			}

			/* Message from the client about some change */
			case XCB_CLIENT_MESSAGE: {

				// Check all windows in general for changes
				// qDebug() << "Window changed (4):" << ((xcb_client_message_event_t*)ev)->window;
				// qDebug() << ( ( xcb_client_message_event_t* )ev )->data;
				break;
			}

			case XCB_SELECTION_CLEAR: {
				// We do nothing here
				qDebug() << "What happened here?";
				break;
			}

			case XCB_KEY_PRESS: {

				// We do nothing here
				xcb_key_press_event_t* kev = (xcb_key_press_event_t*)ev;
				xcb_get_keyboard_mapping_reply_t rep;

				xcb_keysym_t* k = xcb_get_keyboard_mapping_keysyms( &rep );

				quint32 keycode = kev->detail;
				quint32 mods = kev->state & ( ShiftMask | ControlMask | Mod1Mask | Mod2Mask | Mod3Mask | Mod4Mask );

				QSettings keySett( "coreapps", "coreapps" );
				QKeySequence kSeq = keySett.value( "CoreStuff/DockKeyCombo" ).value<QKeySequence>();

				if ( X11KeyParser::calcId( kSeq ) == X11KeyParser::calcId( keycode, mods ) ) {
					mSession->activateDock();
					return true;
				}

				/* 133 is Meta. mods == 16 means no modifiers pressed */
				if ( (keycode == 133 ) and ( mods == 16 ) ) {
					mSession->activateDock();
					return true;
				}

				return false;
			}

			default:{
				// qDebug() << "Some event which we are not watching.";
				break;
			}
		}
	}

	/* We do not want to filter the above events, just intercept them for our work */
	return false;
};
