TEMPLATE = lib
TARGET = corex11
INCLUDEPATH += .

QT += x11extras

# Input
HEADERS += tasksplugin.h
HEADERS += libcorex11.h
SOURCES += libcorex11.cpp

# Silent compilation
CONFIG += silent

# XCB Libs
LIBS    += -lxcb -lxcb-ewmh -lxcb-icccm

# Build section
BUILD_PREFIX = $$(CA_BUILD_DIR)

isEmpty( BUILD_PREFIX ) {
	BUILD_PREFIX = ./build
}

MOC_DIR       = $$BUILD_PREFIX/moc-qt5
OBJECTS_DIR   = $$BUILD_PREFIX/obj-qt5
RCC_DIR       = $$BUILD_PREFIX/qrc-qt5
UI_DIR        = $$BUILD_PREFIX/uic-qt5

unix {
	isEmpty(PREFIX) {
		PREFIX = /usr
	}

	INSTALLS	 += target includes

	contains(DEFINES, LIB64): target.path = $$INSTALL_PREFIX/lib64
	else: target.path = $$INSTALL_PREFIX/lib

	target.path	  = $$PREFIX/lib/

	includes.files    = libcorex11.h
	includes.path     = $$PREFIX/include/corex11/
}
